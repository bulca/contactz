package com.selcukbulca.contactz.ui

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.selcukbulca.contactz.data.source.ContactsRepository
import com.selcukbulca.contactz.util.RxSchedulers
import io.reactivex.schedulers.TestScheduler
import org.junit.Rule
import org.mockito.Mock

open class BaseViewModelTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()
    @Mock
    lateinit var contactsRepository: ContactsRepository

    val testScheduler = TestScheduler()

    val testSchedulers = RxSchedulers(testScheduler, testScheduler)
}