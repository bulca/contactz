package com.selcukbulca.contactz.ui

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import com.selcukbulca.contactz.data.model.Contact
import com.selcukbulca.contactz.data.model.Gender
import com.selcukbulca.contactz.ui.contacts.ContactsViewModel
import com.selcukbulca.contactz.ui.contacts.ContactsViewState
import io.reactivex.Single
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.net.UnknownHostException

@RunWith(JUnit4::class)
class ContactsViewModelTest : BaseViewModelTest() {

    companion object {
        private val DUMMY_CONTACTS = listOf(
            Contact("selcukbulca", "Selcuk", "Bulca", Gender.MALE, ""),
            Contact("nelifaydin", "Elif", "Aydin", Gender.FEMALE, "")
        )
    }

    private lateinit var contactsViewModel: ContactsViewModel

    @Before
    fun setup() {
        contactsRepository = mock {
            on { getContacts() } doReturn Single.fromCallable { DUMMY_CONTACTS }
        }
        contactsViewModel = ContactsViewModel(contactsRepository, testSchedulers)
    }

    @Test
    fun getContactsReturnsLoading() {
        contactsViewModel.getContacts()

        assert(contactsViewModel.viewState.value == ContactsViewState.Loading)
    }

    @Test
    fun getContactsReturnSuccess() {
        contactsViewModel.getContacts()
        testScheduler.triggerActions()

        assert(contactsViewModel.viewState.value is ContactsViewState.Success)
        val successViewState = contactsViewModel.viewState.value as ContactsViewState.Success
        assertThat(successViewState.contacts).hasSize(2)
        assertThat(successViewState.contacts).isEqualTo(DUMMY_CONTACTS)
    }

    @Test
    fun getContactsReturnError() {
        whenever(contactsRepository.getContacts()).thenReturn(Single.error { UnknownHostException() })
        contactsViewModel.getContacts()
        testScheduler.triggerActions()

        assert(contactsViewModel.viewState.value is ContactsViewState.Error)
        val errorViewState = contactsViewModel.viewState.value as ContactsViewState.Error
        assert(errorViewState.throwable is UnknownHostException)
    }
}