package com.selcukbulca.contactz.ui

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import com.selcukbulca.contactz.data.model.ContactDetails
import com.selcukbulca.contactz.ui.contactdetails.ContactDetailsSaveState
import com.selcukbulca.contactz.ui.contactdetails.ContactDetailsViewModel
import com.selcukbulca.contactz.ui.contactdetails.ContactDetailsViewState
import io.reactivex.Completable
import io.reactivex.Single
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.io.IOException

@RunWith(JUnit4::class)
class ContactDetailsViewModelTest : BaseViewModelTest() {

    companion object {
        private val DUMMY_CONTACT_DETAILS =
            ContactDetails("selcukbulca", "selcukbulca@gmail.com", "Istanbul", "0555555555")
    }

    private lateinit var contactDetailsViewModel: ContactDetailsViewModel


    @Before
    fun setup() {
        contactsRepository = mock {
            on { getContactDetails(any()) } doReturn Single.fromCallable { DUMMY_CONTACT_DETAILS }
            on { updateContactDetails(any()) } doReturn Completable.complete()
        }
        contactDetailsViewModel = ContactDetailsViewModel(contactsRepository, testSchedulers)
    }

    @Test
    fun getContactDetailsReturnsLoading() {
        contactDetailsViewModel.getContactDetails("")

        assertThat(contactDetailsViewModel.viewState.value).isEqualTo(ContactDetailsViewState.Loading)
    }

    @Test
    fun getContactDetailsReturnsSuccess() {
        contactDetailsViewModel.getContactDetails("")
        testScheduler.triggerActions()

        assert(contactDetailsViewModel.viewState.value is ContactDetailsViewState.Success)
        val successViewState =
            contactDetailsViewModel.viewState.value as ContactDetailsViewState.Success
        assertThat(successViewState.contactDetails).isEqualTo(DUMMY_CONTACT_DETAILS)
    }

    @Test
    fun getContactDetailsReturnsError() {
        whenever(contactsRepository.getContactDetails("")).thenReturn(Single.error { IOException() })
        contactDetailsViewModel.getContactDetails("")
        testScheduler.triggerActions()

        assert(contactDetailsViewModel.viewState.value is ContactDetailsViewState.Error)
        val errorViewState =
            contactDetailsViewModel.viewState.value as ContactDetailsViewState.Error
        assert(errorViewState.throwable is IOException)
    }

    @Test
    fun saveContactDetailsReturnsSuccess() {
        contactDetailsViewModel.save(DUMMY_CONTACT_DETAILS)
        testScheduler.triggerActions()

        assertThat(contactDetailsViewModel.saveState.value).isEqualTo(ContactDetailsSaveState.Success)
    }

    @Test
    fun saveContactDetailsReturnsError() {
        whenever(contactsRepository.updateContactDetails(any())).thenReturn(
            Completable.error(
                IOException()
            )
        )
        contactDetailsViewModel.save(DUMMY_CONTACT_DETAILS)
        testScheduler.triggerActions()

        assert(contactDetailsViewModel.saveState.value is ContactDetailsSaveState.Error)
        val errorSaveState =
            contactDetailsViewModel.saveState.value as ContactDetailsSaveState.Error
        assert(errorSaveState.throwable is IOException)
    }
}