package com.selcukbulca.contactz.ui

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import com.selcukbulca.contactz.ui.login.LoginViewModel
import com.selcukbulca.contactz.ui.login.LoginViewState
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.net.UnknownHostException

@RunWith(JUnit4::class)
class LoginViewModelTest : BaseViewModelTest() {

    private lateinit var loginViewModel: LoginViewModel

    @Before
    fun setup() {
        contactsRepository = mock()
        loginViewModel = LoginViewModel(contactsRepository, testSchedulers)
    }

    @Test
    fun loginReturnsLoading() {
        whenever(contactsRepository.login(any())).thenReturn(Single.fromCallable { true })
        loginViewModel.login("", "")

        assert(loginViewModel.viewState.value == LoginViewState.Loading)
    }

    @Test
    fun loginReturnsSuccess() {
        whenever(contactsRepository.login(any())).thenReturn(Single.fromCallable { true })
        loginViewModel.login("", "")
        testScheduler.triggerActions()

        assert(loginViewModel.viewState.value == LoginViewState.Success)
    }

    @Test
    fun loginReturnsWrongCredentialsError() {
        whenever(contactsRepository.login(any())).thenReturn(Single.fromCallable { false })
        loginViewModel.login("", "")
        testScheduler.triggerActions()

        assert(loginViewModel.viewState.value is LoginViewState.Failed)
        val failedViewState = loginViewModel.viewState.value as LoginViewState.Failed
        assert(failedViewState.reason === LoginViewState.Reason.WrongCredentials)
    }

    @Test
    fun loginReturnsThrowableError() {
        whenever(contactsRepository.login(any())).thenReturn(Single.error { UnknownHostException() })
        loginViewModel.login("", "")
        testScheduler.triggerActions()

        assert(loginViewModel.viewState.value is LoginViewState.Failed)
        val failedViewState = loginViewModel.viewState.value as LoginViewState.Failed
        assert(failedViewState.reason is LoginViewState.Reason.Error)
        val errorReason = failedViewState.reason as LoginViewState.Reason.Error
        assert(errorReason.throwable is UnknownHostException)
    }
}