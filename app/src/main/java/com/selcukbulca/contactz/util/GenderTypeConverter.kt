package com.selcukbulca.contactz.util

import android.arch.persistence.room.TypeConverter
import com.selcukbulca.contactz.data.model.Gender

object GenderTypeConverter {

    @TypeConverter
    @JvmStatic
    fun toGender(ordinal: Int): Gender {
        return Gender.VALUES[ordinal]
    }

    @TypeConverter
    @JvmStatic
    fun toInt(gender: Gender): Int {
        return gender.ordinal
    }
}