package com.selcukbulca.contactz.util

import io.reactivex.Scheduler

class RxSchedulers(
    val io: Scheduler,
    val ui: Scheduler
)