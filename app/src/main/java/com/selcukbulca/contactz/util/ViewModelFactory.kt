package com.selcukbulca.contactz.util

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.selcukbulca.contactz.data.source.ContactsRepository
import com.selcukbulca.contactz.ui.contactdetails.ContactDetailsViewModel
import com.selcukbulca.contactz.ui.contacts.ContactsViewModel
import com.selcukbulca.contactz.ui.login.LoginViewModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ViewModelFactory @Inject constructor(
    private val repository: ContactsRepository,
    private val schedulers: RxSchedulers
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(repository, schedulers) as T
        } else if (modelClass.isAssignableFrom(ContactsViewModel::class.java)) {
            return ContactsViewModel(repository, schedulers) as T
        } else if (modelClass.isAssignableFrom(ContactDetailsViewModel::class.java)) {
            return ContactDetailsViewModel(repository, schedulers) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}