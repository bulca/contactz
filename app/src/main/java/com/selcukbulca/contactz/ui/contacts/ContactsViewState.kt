package com.selcukbulca.contactz.ui.contacts

import com.selcukbulca.contactz.data.model.Contact

sealed class ContactsViewState {
    object Loading : ContactsViewState()
    class Success(val contacts: List<Contact>) : ContactsViewState()
    class Error(val throwable: Throwable) : ContactsViewState()
}