package com.selcukbulca.contactz.ui.contactdetails

import android.arch.lifecycle.MutableLiveData
import com.selcukbulca.contactz.data.model.ContactDetails
import com.selcukbulca.contactz.data.source.ContactsRepository
import com.selcukbulca.contactz.util.RxSchedulers
import com.selcukbulca.contactz.util.RxViewModel
import com.selcukbulca.contactz.util.SingleLiveEvent
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy

class ContactDetailsViewModel(
    private val repository: ContactsRepository,
    private val schedulers: RxSchedulers
) : RxViewModel() {

    val viewState = MutableLiveData<ContactDetailsViewState>()
    val saveState = SingleLiveEvent<ContactDetailsSaveState>()

    fun getContactDetails(username: String) {
        viewState.value = ContactDetailsViewState.Loading
        disposables += repository.getContactDetails(username)
            .subscribeOn(schedulers.io)
            .observeOn(schedulers.ui)
            .subscribeBy(onSuccess = {
                viewState.value = ContactDetailsViewState.Success(it)
            }, onError = {
                viewState.value = ContactDetailsViewState.Error(it)
            })
    }

    fun save(contactDetails: ContactDetails) {
        saveState.value = ContactDetailsSaveState.InProgress
        disposables += repository.updateContactDetails(contactDetails)
            .subscribeOn(schedulers.io)
            .observeOn(schedulers.ui)
            .subscribeBy(onComplete = {
                saveState.value = ContactDetailsSaveState.Success
            }, onError = {
                saveState.value = ContactDetailsSaveState.Error(it)
            })
    }
}