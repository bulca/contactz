package com.selcukbulca.contactz.ui.contacts

import android.arch.lifecycle.MutableLiveData
import com.selcukbulca.contactz.data.source.ContactsRepository
import com.selcukbulca.contactz.ui.contacts.ContactsViewState.*
import com.selcukbulca.contactz.util.RxSchedulers
import com.selcukbulca.contactz.util.RxViewModel
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy

class ContactsViewModel(
    private val repository: ContactsRepository,
    private val schedulers: RxSchedulers
) : RxViewModel() {

    val viewState = MutableLiveData<ContactsViewState>()

    fun getContacts() {
        viewState.value = Loading
        disposables += repository.getContacts()
            .subscribeOn(schedulers.io)
            .observeOn(schedulers.ui)
            .subscribeBy(onSuccess = {
                viewState.value = Success(it)
            }, onError = {
                viewState.value = Error(it)
            })
    }
}