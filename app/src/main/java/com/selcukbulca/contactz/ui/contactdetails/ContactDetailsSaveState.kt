package com.selcukbulca.contactz.ui.contactdetails

sealed class ContactDetailsSaveState {
    object InProgress : ContactDetailsSaveState()
    object Success : ContactDetailsSaveState()
    class Error(val throwable: Throwable) : ContactDetailsSaveState()
}