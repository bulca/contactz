package com.selcukbulca.contactz.ui.login

import android.arch.lifecycle.MutableLiveData
import com.selcukbulca.contactz.data.model.ContactCredentials
import com.selcukbulca.contactz.data.source.ContactsRepository
import com.selcukbulca.contactz.ui.login.LoginViewState.*
import com.selcukbulca.contactz.ui.login.LoginViewState.Reason.Error
import com.selcukbulca.contactz.ui.login.LoginViewState.Reason.WrongCredentials
import com.selcukbulca.contactz.util.RxSchedulers
import com.selcukbulca.contactz.util.RxViewModel
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val repository: ContactsRepository,
    private val schedulers: RxSchedulers
) : RxViewModel() {

    val viewState = MutableLiveData<LoginViewState>()

    fun login(username: String, password: String) {
        viewState.value = Loading
        disposables += repository.login(ContactCredentials(username, password))
            .subscribeOn(schedulers.io)
            .observeOn(schedulers.ui)
            .subscribeBy(onSuccess = {
                viewState.value = when (it) {
                    true -> Success
                    false -> Failed(WrongCredentials)
                }
            }, onError = {
                viewState.value = Failed(Error(it))
            })
    }
}