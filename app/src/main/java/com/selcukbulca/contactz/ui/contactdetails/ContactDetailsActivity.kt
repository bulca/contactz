package com.selcukbulca.contactz.ui.contactdetails

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import androidx.core.widget.toast
import com.selcukbulca.contactz.ContactzApp
import com.selcukbulca.contactz.R
import com.selcukbulca.contactz.data.model.Contact
import com.selcukbulca.contactz.data.model.ContactDetails
import com.selcukbulca.contactz.ext.*
import com.selcukbulca.contactz.util.ViewModelFactory
import kotlinx.android.synthetic.main.activity_contact_details.*
import kotlinx.android.synthetic.main.item_contact.*
import javax.inject.Inject

class ContactDetailsActivity : AppCompatActivity() {

    companion object {
        private const val KEY_CONTACT = "contact"

        fun start(context: Context, contact: Contact) =
            context.startActivity(Intent(context, ContactDetailsActivity::class.java).apply {
                putExtra(KEY_CONTACT, contact)
            })
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: ContactDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_details)
        injectDependencies()

        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.title = getString(R.string.title_contact_details)
        }

        val contact = intent.getParcelableExtra<Contact>(KEY_CONTACT)
        renderContact(contact)

        emailEditText.onTextChanged { saveButton.isEnabled = it.isNotEmpty() }
        phoneNumberEditText.onTextChanged { saveButton.isEnabled = it.isNotEmpty() }
        addressEditText.onTextChanged { saveButton.isEnabled = it.isNotEmpty() }

        saveButton.setOnClickListener {
            val contactDetails = ContactDetails(
                contact.username,
                emailEditText.asString,
                addressEditText.asString,
                phoneNumberEditText.asString
            )
            viewModel.save(contactDetails)
            contactDetailsLayout.requestFocus()
            hideKeyboard()
        }

        viewModel = ViewModelProviders.of(this, viewModelFactory).get()
        viewModel.viewState.observe(this) { it?.let { renderViewState(it) } }
        viewModel.saveState.observe(this) { it?.let { renderSaveState(it) } }

        if (savedInstanceState == null) {
            viewModel.getContactDetails(contact.username)
        }
    }

    private fun injectDependencies() {
        (application as ContactzApp).appComponent.inject(this)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun renderContact(contact: Contact) {
        imageView.setImageUrl(contact.imageUrl)
        nameTextView.text =
                getString(R.string.contact_firstName_lastName, contact.firstName, contact.lastName)
        usernameGenderTextView.text =
                getString(R.string.contact_username_gender, contact.username, contact.gender)
    }

    private fun renderViewState(viewState: ContactDetailsViewState) {
        when (viewState) {
            is ContactDetailsViewState.Loading -> renderLoading()
            is ContactDetailsViewState.Success -> renderSuccess(viewState)
            is ContactDetailsViewState.Error -> renderError(viewState.throwable)
        }
    }

    private fun renderLoading() {
        loadingProgressBar.show()
        contactDetailsLayout.hide()
    }

    private fun renderSuccess(viewState: ContactDetailsViewState.Success) {
        loadingProgressBar.hide()
        contactDetailsLayout.show()
        viewState.apply {
            emailEditText.setText(contactDetails.email)
            phoneNumberEditText.setText(contactDetails.phoneNumber)
            addressEditText.setText(contactDetails.address)
        }
    }

    private fun renderError(throwable: Throwable) {
        // Error handling could be improved
        loadingProgressBar.hide()
        toast(R.string.error_generic, duration = Toast.LENGTH_LONG)
    }

    private fun renderSaveState(saveState: ContactDetailsSaveState) {
        saveButton.isEnabled = saveState != ContactDetailsSaveState.InProgress
        when (saveState) {
            is ContactDetailsSaveState.Success -> toast(R.string.success_contact_details_save)
            is ContactDetailsSaveState.Error -> toast(R.string.error_contact_details_save)
        }
    }
}