package com.selcukbulca.contactz.ui.contacts

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.selcukbulca.contactz.R
import com.selcukbulca.contactz.data.model.Contact
import com.selcukbulca.contactz.ext.inflate

class ContactsAdapter(
    private val contacts: List<Contact>,
    private val onContactClicked: (Contact) -> Unit
) : RecyclerView.Adapter<ContactViewHolder>() {

    override fun getItemCount() = contacts.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ContactViewHolder(parent.inflate(R.layout.item_contact), onContactClicked)

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) =
        holder.bind(contacts[position])

    override fun onViewRecycled(holder: ContactViewHolder) {
        holder.recycle()
    }
}