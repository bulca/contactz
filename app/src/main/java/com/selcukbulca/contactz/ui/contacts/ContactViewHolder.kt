package com.selcukbulca.contactz.ui.contacts

import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import com.selcukbulca.contactz.R
import com.selcukbulca.contactz.data.model.Contact
import com.selcukbulca.contactz.ext.setImageUrl
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_contact.*
import kotlinx.android.synthetic.main.item_contact.view.*

class ContactViewHolder(
    override val containerView: View,
    private val onContactClicked: (Contact) -> Unit
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bind(contact: Contact) {
        containerView.apply {
            imageView.setImageUrl(contact.imageUrl)
            nameTextView.text = context.getString(
                R.string.contact_firstName_lastName,
                contact.firstName,
                contact.lastName
            )
            usernameGenderTextView.text = context.getString(
                R.string.contact_username_gender,
                contact.username,
                contact.gender
            )
            setOnClickListener { onContactClicked.invoke(contact) }
        }
    }

    fun recycle() {
        Glide.clear(imageView)
    }
}