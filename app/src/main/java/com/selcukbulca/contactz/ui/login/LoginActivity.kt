package com.selcukbulca.contactz.ui.login

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import androidx.core.widget.toast
import com.selcukbulca.contactz.ContactzApp
import com.selcukbulca.contactz.R
import com.selcukbulca.contactz.ext.asString
import com.selcukbulca.contactz.ext.get
import com.selcukbulca.contactz.ext.observe
import com.selcukbulca.contactz.ext.onTextChanged
import com.selcukbulca.contactz.ui.contacts.ContactsActivity
import com.selcukbulca.contactz.ui.login.LoginViewState.*
import com.selcukbulca.contactz.ui.login.LoginViewState.Reason.Error
import com.selcukbulca.contactz.ui.login.LoginViewState.Reason.WrongCredentials
import com.selcukbulca.contactz.util.ViewModelFactory
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        injectDependencies()

        supportActionBar?.title = getString(R.string.title_login)

        loginButton.setOnClickListener {
            viewModel.login(usernameEditText.asString, passwordEditText.asString)
        }

        usernameEditText.onTextChanged { maybeEnableLoginButton() }
        passwordEditText.onTextChanged { maybeEnableLoginButton() }

        viewModel = ViewModelProviders.of(this, viewModelFactory).get()
        viewModel.viewState.observe(this) { it?.let { renderViewState(it) } }
    }

    private fun injectDependencies() {
        (application as ContactzApp).appComponent.inject(this)
    }

    private fun maybeEnableLoginButton() {
        loginButton.isEnabled =
                usernameEditText.text.isNotEmpty() && passwordEditText.text.isNotEmpty()
    }

    private fun renderViewState(viewState: LoginViewState) = when (viewState) {
        is Loading -> renderLoading()
        is Failed -> renderFailed(viewState)
        is Success -> navigateToContacts()
    }

    private fun renderLoading() {
        usernameLayout.isEnabled = false
        passwordLayout.isEnabled = false
        loginButton.isEnabled = false
    }

    private fun renderFailed(viewState: Failed) {
        usernameLayout.isEnabled = true
        passwordLayout.isEnabled = true
        loginButton.isEnabled = true

        when (viewState.reason) {
            is WrongCredentials -> {
                toast(R.string.error_wrong_credentials, duration = Toast.LENGTH_LONG)
            }
            is Error -> {
                // Error handling could be improved
                toast(R.string.error_generic, duration = Toast.LENGTH_LONG)
            }
        }
    }

    private fun navigateToContacts() {
        ContactsActivity.start(this)
        finish()
    }
}