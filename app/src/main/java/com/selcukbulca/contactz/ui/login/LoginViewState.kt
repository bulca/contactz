package com.selcukbulca.contactz.ui.login

sealed class LoginViewState {
    object Loading : LoginViewState()
    object Success : LoginViewState()
    class Failed(val reason: Reason) : LoginViewState()

    sealed class Reason {
        object WrongCredentials : Reason()
        class Error(val throwable: Throwable) : Reason()
    }
}