package com.selcukbulca.contactz.ui.contactdetails

import com.selcukbulca.contactz.data.model.ContactDetails

sealed class ContactDetailsViewState {
    object Loading : ContactDetailsViewState()
    class Success(val contactDetails: ContactDetails) : ContactDetailsViewState()
    class Error(val throwable: Throwable) : ContactDetailsViewState()
}