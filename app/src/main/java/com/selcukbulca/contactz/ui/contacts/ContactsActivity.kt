package com.selcukbulca.contactz.ui.contacts

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import com.selcukbulca.contactz.ContactzApp
import com.selcukbulca.contactz.R
import com.selcukbulca.contactz.ext.get
import com.selcukbulca.contactz.ext.hide
import com.selcukbulca.contactz.ext.observe
import com.selcukbulca.contactz.ext.show
import com.selcukbulca.contactz.ui.contactdetails.ContactDetailsActivity
import com.selcukbulca.contactz.ui.contacts.ContactsViewState.*
import com.selcukbulca.contactz.util.ViewModelFactory
import kotlinx.android.synthetic.main.activity_contacts.*
import javax.inject.Inject

class ContactsActivity : AppCompatActivity() {

    companion object {
        fun start(context: Context) =
            context.startActivity(Intent(context, ContactsActivity::class.java))
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: ContactsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacts)
        injectDependencies()

        supportActionBar?.title = getString(R.string.title_contacts)

        setupRecyclerView()

        retryButton.setOnClickListener { viewModel.getContacts() }

        viewModel = ViewModelProviders.of(this, viewModelFactory).get()
        viewModel.viewState.observe(this) { it?.let { renderViewState(it) } }

        if (savedInstanceState == null || viewModel.viewState.value == null) {
            viewModel.getContacts()
        }
    }

    private fun injectDependencies() {
        (application as ContactzApp).appComponent.inject(this)
    }

    private fun setupRecyclerView() {
        val itemDecoration = DividerItemDecoration(this, LinearLayoutManager.VERTICAL).apply {
            setDrawable(
                ContextCompat.getDrawable(
                    this@ContactsActivity,
                    R.drawable.divider_contacts_list
                )!!
            )
        }
        contactList.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ContactsActivity)
            addItemDecoration(itemDecoration)
        }
    }

    private fun renderViewState(viewState: ContactsViewState) {
        return when (viewState) {
            is Loading -> renderLoading()
            is Success -> renderSuccess(viewState)
            is Error -> renderError(viewState.throwable)
        }
    }

    private fun renderLoading() {
        loadingProgressBar.show()
        contactList.hide()
        errorLayout.hide()
    }

    private fun renderError(throwable: Throwable) {
        // Error handling could be improved
        loadingProgressBar.hide()
        contactList.hide()
        errorLayout.show()

        errorTextView.text = getString(R.string.error_generic)
    }

    private fun renderSuccess(viewState: Success) {
        loadingProgressBar.hide()
        errorLayout.hide()
        contactList.apply {
            show()
            adapter = ContactsAdapter(viewState.contacts) { contact ->
                ContactDetailsActivity.start(this@ContactsActivity, contact)
            }
        }
    }
}