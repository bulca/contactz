package com.selcukbulca.contactz.data.source.remote

import android.content.res.Resources
import android.support.annotation.RawRes
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.selcukbulca.contactz.R
import com.selcukbulca.contactz.data.model.Contact
import com.selcukbulca.contactz.data.model.ContactCredentials
import com.selcukbulca.contactz.data.model.ContactDetails
import io.reactivex.Single
import java.util.ArrayList

class MockContactsService(private val resources: Resources, val gson: Gson) : ContactsService {
    override fun login(contactCredentials: ContactCredentials): Single<Boolean> {
        return Single.fromCallable {
            parse<ArrayList<ContactCredentials>>(R.raw.contacts_credentials_mock)
                .any { it == contactCredentials }
        }
    }

    override fun getContacts(): Single<List<Contact>> {
        return Single.fromCallable {
            parse<ArrayList<Contact>>(R.raw.contacts_mock)
        }
    }

    override fun getContactDetails(username: String): Single<ContactDetails> {
        return Single.fromCallable {
            parse<ArrayList<ContactDetails>>(R.raw.contact_details_mock)
                .first { it.username == username }
        }
    }

    private inline fun <reified T> parse(@RawRes rawResource: Int): T {
        return resources.openRawResource(rawResource)
            .bufferedReader()
            .use {
                it.readText()
            }.run {
                gson.fromJson<T>(this, object : TypeToken<T>() {}.type)
            }
    }
}