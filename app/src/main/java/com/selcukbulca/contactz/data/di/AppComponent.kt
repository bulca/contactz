package com.selcukbulca.contactz.data.di

import com.selcukbulca.contactz.ui.contactdetails.ContactDetailsActivity
import com.selcukbulca.contactz.ui.contacts.ContactsActivity
import com.selcukbulca.contactz.ui.login.LoginActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        NetworkModule::class,
        DbModule::class
    ]
)
interface AppComponent {
    fun inject(loginActivity: LoginActivity)
    fun inject(contactsActivity: ContactsActivity)
    fun inject(contactDetailsActivity: ContactDetailsActivity)
}