package com.selcukbulca.contactz.data.di

import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.selcukbulca.contactz.util.RxSchedulers
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

@Module
class AppModule(private val application: Application) {

    @Provides
    fun provideContext(): Context {
        return application.applicationContext
    }

    @Provides
    fun provideResources(context: Context): Resources {
        return context.resources
    }

    @Provides
    @Singleton
    fun provideRxSchedulers(): RxSchedulers {
        return RxSchedulers(
            Schedulers.io(),
            AndroidSchedulers.mainThread()
        )
    }
}