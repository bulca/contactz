package com.selcukbulca.contactz.data.model

data class ContactCredentials(
    val username: String,
    val password: String
)