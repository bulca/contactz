package com.selcukbulca.contactz.data.di

import android.content.res.Resources
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.selcukbulca.contactz.data.source.remote.ContactsService
import com.selcukbulca.contactz.data.source.remote.MockContactsService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()
    }

    @Provides
    @Singleton
    fun provideContactsService(resources: Resources, gson: Gson): ContactsService {
        return MockContactsService(resources, gson)
    }
}