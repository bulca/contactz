package com.selcukbulca.contactz.data.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "contact_details")
data class ContactDetails(
    @PrimaryKey
    val username: String,
    val email: String,
    val address: String,
    @SerializedName("phone")
    var phoneNumber: String
)