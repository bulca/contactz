package com.selcukbulca.contactz.data.source.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import com.selcukbulca.contactz.data.model.Contact
import com.selcukbulca.contactz.data.model.ContactDetails
import io.reactivex.Single

@Dao
interface ContactsDao {
    @Query("SELECT * FROM contacts")
    fun getContacts(): Single<List<Contact>>

    @Insert(onConflict = REPLACE)
    fun insertContacts(contacts: List<Contact>)

    @Query("SELECT * FROM contact_details WHERE username = :username LIMIT 1")
    fun getContactDetails(username: String): Single<ContactDetails>

    @Insert(onConflict = REPLACE)
    fun insertContactDetails(contactDetails: ContactDetails)

    @Update(onConflict = REPLACE)
    fun updateContactDetails(contactDetails: ContactDetails)
}