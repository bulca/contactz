package com.selcukbulca.contactz.data.source.remote

import com.selcukbulca.contactz.data.model.Contact
import com.selcukbulca.contactz.data.model.ContactCredentials
import com.selcukbulca.contactz.data.model.ContactDetails
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ContactsRemoteDataSource @Inject constructor(
    private val contactsService: ContactsService
) {

    companion object {
        // Simulate network calls by delaying observables
        const val DELAY_NETWORK_MS = 1000L
    }

    fun login(contactCredentials: ContactCredentials): Single<Boolean> {
        return contactsService.login(contactCredentials)
            .delay(DELAY_NETWORK_MS, TimeUnit.MILLISECONDS)
    }

    fun getContacts(): Single<List<Contact>> {
        return contactsService.getContacts()
            .delay(DELAY_NETWORK_MS, TimeUnit.MILLISECONDS)
    }

    fun getContactDetails(username: String): Single<ContactDetails> {
        return contactsService.getContactDetails(username)
            .delay(DELAY_NETWORK_MS, TimeUnit.MILLISECONDS)
    }
}