package com.selcukbulca.contactz.data.di

import android.arch.persistence.room.Room
import android.content.Context
import com.selcukbulca.contactz.data.source.local.ContactsDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule {

    @Provides
    @Singleton
    fun provideContactsDatabase(context: Context): ContactsDatabase {
        return Room.databaseBuilder(context, ContactsDatabase::class.java, "contacts-db")
            .fallbackToDestructiveMigration()
            .build()
    }
}