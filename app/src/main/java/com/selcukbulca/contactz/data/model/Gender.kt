package com.selcukbulca.contactz.data.model

import com.google.gson.annotations.SerializedName

enum class Gender {
    @SerializedName("Male")
    MALE,
    @SerializedName("Female")
    FEMALE;

    companion object {
        val VALUES = values()
    }
}