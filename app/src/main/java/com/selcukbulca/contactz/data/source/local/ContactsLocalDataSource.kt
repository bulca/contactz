package com.selcukbulca.contactz.data.source.local

import com.selcukbulca.contactz.data.model.Contact
import com.selcukbulca.contactz.data.model.ContactDetails
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ContactsLocalDataSource @Inject constructor(
    contactsDatabase: ContactsDatabase
) {

    private val contactsDao = contactsDatabase.contactsDao()

    fun getContacts(): Single<List<Contact>> {
        return contactsDao.getContacts()
    }

    fun saveContacts(contacts: List<Contact>) {
        return contactsDao.insertContacts(contacts)
    }

    fun getContactDetails(username: String): Single<ContactDetails> {
        return contactsDao.getContactDetails(username)
    }

    fun saveContactDetails(contactDetails: ContactDetails) {
        return contactsDao.insertContactDetails(contactDetails)
    }

    fun updateContactDetails(contactDetails: ContactDetails) {
        return contactsDao.updateContactDetails(contactDetails)
    }
}