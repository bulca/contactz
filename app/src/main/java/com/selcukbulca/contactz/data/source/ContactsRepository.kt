package com.selcukbulca.contactz.data.source

import com.selcukbulca.contactz.data.model.Contact
import com.selcukbulca.contactz.data.model.ContactCredentials
import com.selcukbulca.contactz.data.model.ContactDetails
import com.selcukbulca.contactz.data.source.local.ContactsLocalDataSource
import com.selcukbulca.contactz.data.source.remote.ContactsRemoteDataSource
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ContactsRepository @Inject constructor(
    private val localDataSource: ContactsLocalDataSource,
    private val remoteDataSource: ContactsRemoteDataSource
) : ContactsDataSource {

    override fun login(contactCredentials: ContactCredentials): Single<Boolean> {
        return remoteDataSource.login(contactCredentials)
    }

    override fun getContacts(): Single<List<Contact>> {
        val remoteContacts = remoteDataSource.getContacts()
            .doOnSuccess { localDataSource.saveContacts(it) }
        val localContacts = localDataSource.getContacts()
        return Single.concat(localContacts, remoteContacts)
            .filter { !it.isEmpty() }
            .firstOrError()
    }

    override fun getContactDetails(username: String): Single<ContactDetails> {
        val remoteContactDetails = remoteDataSource.getContactDetails(username)
            .doOnSuccess { localDataSource.saveContactDetails(it) }
        val localContactDetails = localDataSource.getContactDetails(username)
        return localContactDetails.onErrorResumeNext { remoteContactDetails }
    }

    override fun updateContactDetails(contactDetails: ContactDetails): Completable {
        return Completable.fromCallable {
            localDataSource.updateContactDetails(contactDetails)
        }
    }
}