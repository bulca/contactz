package com.selcukbulca.contactz.data.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "contacts")
@Parcelize
data class Contact(
    @PrimaryKey
    val username: String,
    val firstName: String,
    val lastName: String,
    val gender: Gender,
    val imageUrl: String
) : Parcelable