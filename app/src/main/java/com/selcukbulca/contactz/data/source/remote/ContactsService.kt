package com.selcukbulca.contactz.data.source.remote

import com.selcukbulca.contactz.data.model.Contact
import com.selcukbulca.contactz.data.model.ContactCredentials
import com.selcukbulca.contactz.data.model.ContactDetails
import io.reactivex.Single

interface ContactsService {
    fun login(contactCredentials: ContactCredentials): Single<Boolean>
    fun getContacts(): Single<List<Contact>>
    fun getContactDetails(username: String): Single<ContactDetails>
}