package com.selcukbulca.contactz.data.source.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.selcukbulca.contactz.data.model.Contact
import com.selcukbulca.contactz.data.model.ContactDetails
import com.selcukbulca.contactz.util.GenderTypeConverter

@Database(entities = [Contact::class, ContactDetails::class], version = 1, exportSchema = false)
@TypeConverters(GenderTypeConverter::class)
abstract class ContactsDatabase : RoomDatabase() {
    abstract fun contactsDao(): ContactsDao
}