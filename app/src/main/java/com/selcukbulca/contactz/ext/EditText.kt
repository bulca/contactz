package com.selcukbulca.contactz.ext

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

inline val EditText.asString
    get() = text.toString()

inline fun EditText.onTextChanged(crossinline block: (String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(text: Editable) {
            // no-op
        }

        override fun beforeTextChanged(text: CharSequence, start: Int, count: Int, after: Int) {
            // no-op
        }

        override fun onTextChanged(text: CharSequence, start: Int, before: Int, count: Int) {
            block.invoke(text.toString())
        }
    })
}