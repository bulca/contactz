package com.selcukbulca.contactz.ext

import android.view.View
import androidx.core.view.isGone
import androidx.core.view.isVisible

fun View.hide() {
    if (!this.isGone) {
        visibility = View.GONE
    }
}

fun View.show() {
    if (!this.isVisible) {
        visibility = View.VISIBLE
    }
}