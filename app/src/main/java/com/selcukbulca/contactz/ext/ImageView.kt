package com.selcukbulca.contactz.ext

import android.graphics.drawable.ColorDrawable
import android.support.v4.content.ContextCompat
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.selcukbulca.contactz.R

fun ImageView.setImageUrl(imageUrl: String) {
    Glide.with(context)
        .load(imageUrl)
        .placeholder(ColorDrawable(ContextCompat.getColor(context, R.color.material_grey_200)))
        .into(this)
}