package com.selcukbulca.contactz

import android.app.Application
import com.selcukbulca.contactz.data.di.AppComponent
import com.selcukbulca.contactz.data.di.AppModule
import com.selcukbulca.contactz.data.di.DaggerAppComponent
import timber.log.Timber
import kotlin.LazyThreadSafetyMode.NONE

class ContactzApp : Application() {

    val appComponent: AppComponent by lazy(NONE) {
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}